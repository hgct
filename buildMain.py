#!/usr/bin/env python

import sys, os.path, re, imp, py_compile

if len(sys.argv) < 3:
    print "Usage: <out-file> <main-file> [<modules>...]"
    sys.exit(1)

outFile = sys.argv[1]
mainFile = sys.argv[2]
moduleFiles = sys.argv[3:]

preamble = """#!/usr/bin/env python

import tempfile, sys, imp, __builtin__

modules = {}

class Module:
    pass

def addModule(name, contents, descr):
    m = Module()
    m.descr = descr
    m.contents = contents
    modules[name] = m

origImport = __import__
def MyImport(name, globals=None, locals=None, fromlist=None):
    try:
        return sys.modules[name]
    except KeyError:
        pass

    if modules.has_key(name):
        f = tempfile.TemporaryFile()
        f.write(modules[name].contents)
        f.seek(0)
        ret = imp.load_module(name, f, '', modules[name].descr)
        f.close()
        return ret
    else:
        return origImport(name, globals, locals, fromlist)

__builtin__.__import__ = MyImport
"""

def findDescr(suffix):
    for x in imp.get_suffixes():
        if x[0] == '.' + suffix:
            return x

fout = open(outFile, 'w')
fout.write(preamble)
REFile = re.compile('^(.*)\\.(.*?)$')

for f in moduleFiles + [mainFile]:
    py_compile.compile(f)
    fin = open(f + 'c', 'r')
    name = os.path.basename(f)
    m = REFile.match(name)
    fout.write('addModule(' + repr(m.group(1)) + ', ' + repr(fin.read()) + ', ' + repr(findDescr('pyc')) + ')\n')

m = REFile.match(mainFile)
fout.write('import ctcore\n')
fout.write('ctcore.executeMain = False\n')
fout.write('import ' + m.group(1) + '\n')
fout.write(m.group(1) + '.main()\n')
fout.close()
