PREFIX=$(HOME)
BIN=$(PREFIX)/bin

MODULES=mysubprocess.py ctcore.py hg.py git.py settings.py commit.py
MAIN=main.py
EXECUTABLE=gct
INSTALL=install

all: gct hgct

devel:
	ln -sf $(MAIN) $(EXECUTABLE)
	ln -sf $(MAIN) hgct

gct: $(MAIN) $(MODULES) buildMain.py
	rm -f $(EXECUTABLE)
	./buildMain.py $(EXECUTABLE) $(MAIN) $(MODULES)
	chmod +x $(EXECUTABLE)

hgct: gct
	ln -sf $(EXECUTABLE) hgct

clean:
	rm -f *~ *.pyc $(EXECUTABLE) hgct

install: gct
	$(INSTALL) $(EXECUTABLE) $(BIN)
	cd $(BIN) ; ln -sf $(EXECUTABLE) hgct

.PHONY: all devel clean
