#!/usr/bin/env python

# hct - hg extension for invoking the (h)gct commit tool

# This is currently very simple but allows the (h)gct commit tool to
# integrate with the hg commandline interface.  By default, the hgct
# command should be in the user's path.  Edit the HGCT_PATH variable
# to manually specify a different path.
#
# To enable this extension, add a line like the following to the
# extensions section in your .hgrc file:
#
# [extensions]
# ...
# hct = /path/to/hct.py
#
# If you have installed hct.py in the standard Python module directory
# (typically something like /usr/lib/python2.4/site-packages) you can
# leave the path empty. That is, add the following line to .hgrc
# instead:
#
# [extensions]
# ...
# hct =
#
# Invoke the commit tool with the "hg ct" command.  It will appear in
# hg help as normal.
#

HGCT_PATH = "hgct"

import os

def invoke_hgct(ui, repo):
    """Run hgct commit tool"""

    os.chdir(repo.root)
    status = os.system(HGCT_PATH + " --one-shot")

    if status is not 0:
        print "Error invoking hgct commit tool"

cmdtable = { 'ct' : (invoke_hgct, [], 'hg ct') }

def reposetup(ui, repo):
    pass
